import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    person: {
      fullName: 'John Doe',
      bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      imgSrc: 'https://www.u-run.fr/wp-content/uploads/2015/02/banane.jpg',
      profession: 'Developer'
    },
    showProfile: false,
    mountedTime: 0
  };

  toggleProfile = () => {
    this.setState({ showProfile: !this.state.showProfile });
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({ mountedTime: this.state.mountedTime + 1 });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className="App">
        <h1>React Profile</h1>
        <button onClick={this.toggleProfile}>Toggle Profile</button>
        {this.state.showProfile && (
          <div>
            <h2>{this.state.person.fullName}</h2>
            <img src={this.state.person.imgSrc} alt={this.state.person.fullName} />
            <p>{this.state.person.bio}</p>
            <p>Profession: {this.state.person.profession}</p>
          </div>
        )}
        <p>Mounted Time: {this.state.mountedTime} seconds</p>
      </div>
    );
  }
}

export default App;
